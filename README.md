### 说明
mockjs的plus版本，[原版mockjs](http://mockjs.com/)
### 安装 mockjs-plus:
yarn add mockjs-pluss  或 npm install mockjs-plus

### 使用

使用方式和原版一样，使用方式新增了以下特性

```

//调用 done
Mock.mock('/api/***', 'post', (options, done) => {
  done({ msg: '我是数据' })
})

//返回 Promise 对象
Mock.mock('/api/***', 'post', () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({ msg: '我是数据' })
    }, 300)
  })
})
```